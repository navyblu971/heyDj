import React from 'react';
import ReactDOM from 'react-dom';

class RequestSong extends React.Component {
    constructor(props) {
      super(props);
   
      this.state = {
        url: "" , 
        barId: props.barId
      };
    }
   
    handleSubmitForm(event) {
      alert("Full Name: " + this.state.url + "\nbarId  :" + this.state.barId);
      event.preventDefault();
    }
   
    handleChange(event) {
      var value = event.target.value;
   
      this.setState({
        url: value
      });
    }
   
    render() {
      return (
        <form onSubmit={event => this.handleSubmitForm(event)}>
          <label>
            Send url to your favourite bar..:
            <input
              type="text"
              value={this.state.url}
              onChange={event => this.handleChange(event)}
            />
          </label>
          <input type="submit" value="Submit" />
          <input type="hidden" value={this.state.barId} />
          <p>{this.state.url}</p>        
        </form>
      );
    }
  }
    
   
  // Render
  //ReactDOM.render(<RequestSong />, document.getElementById("formRequest"));

  export default RequestSong;